---
title: "Assignment 1 - Language Development in ASD - part 3"
author: "Signe Kirk Brødbæk"
date: "Oktober 3, 2018"
output:
  word_document: default
  html_document: default
---

```{r setup, include=FALSE}
library(pacman)
p_load(tidyverse,stringr,lme4,MuMIn,pastecs,Metrics,caret)
setwd("~/Dropbox/AU/3rd semester/EM III/R/EM 3 Assignments/Assignment1")
```

## Welcome to the third exciting part of the Language Development in ASD exercise

In this exercise, we will delve more in depth with different practices of model comparison and model selection, by first evaluating your models from last time, then learning how to cross-validate models and finally how to systematically compare models.

N.B. There are several datasets for this exercise, so pay attention to which one you are using!

1. The (training) dataset from last time (the awesome one you produced :-) ).
2. The (test) datasets on which you can test the models from last time:
* Demographic and clinical data: https://www.dropbox.com/s/ra99bdvm6fzay3g/demo_test.csv?dl=1
* Utterance Length data: https://www.dropbox.com/s/uxtqqzl18nwxowq/LU_test.csv?dl=1
* Word data: https://www.dropbox.com/s/1ces4hv8kh0stov/token_test.csv?dl=1


```{r,include=FALSE,echo=FALSE}
# Create test data
demo_test <- read.csv("demo_test.csv")
token_test <- read.csv("token_test.csv")
lu_test <- read.csv("LU_test.csv")

colnames(demo_test)[1] <- "SUBJ" 
colnames(demo_test)[2] <- "VISIT" 

lu_test$VISIT <- as.integer(str_extract(lu_test$VISIT,"[:digit:]"))
token_test$VISIT <- as.integer(str_extract(lu_test$VISIT,"[:digit:]"))

lu_test$SUBJ <- str_replace_all(lu_test$SUBJ,"[:punct:]","")
token_test$SUBJ <- str_replace_all(token_test$SUBJ,"[:punct:]","")
demo_test$SUBJ <- str_replace_all(demo_test$SUBJ,"[:punct:]","")

lu_test <- select(lu_test, SUBJ, VISIT, MOT_MLU, MOT_LUstd, CHI_MLU, CHI_LUstd)
token_test <- select(token_test, SUBJ, VISIT, types_MOT, types_CHI, tokens_MOT, tokens_CHI)
demo_test <- select(demo_test,SUBJ,VISIT, Ethnicity, Diagnosis, Gender, Age, ADOS,  MullenRaw, ExpressiveLangRaw)

test <- full_join(demo_test,lu_test)
test <- full_join(test,token_test)

colnames(test)[8] <- "nonverbalIQ" 
colnames(test)[9] <- "verbalIQ" 

visit1 <- subset(test,VISIT == 1,select= c(SUBJ,ADOS,nonverbalIQ,verbalIQ))
visit1 <- filter(test,VISIT == "1") %>% select(SUBJ,ADOS,nonverbalIQ,verbalIQ)

colnames(visit1)[2] <- "ADOS1" 
colnames(visit1)[3] <- "nonverbalIQ1"
colnames(visit1)[4] <- "verbalIQ1" 

# Merge subset to full dataset 
test_final <- full_join(test,visit1)

# Remove "old" variables ADOS, nonverbalIQ, and verbalIQ
test_final <- test_final[-c(7,8,9)]

# Make the "right" order of columns
test_final <- test_final[c(1,4,6,5,3,2,15,16,17,7:14)]

# Change Gender to F and M
test_final$Gender <- as.factor(test_final$Gender)
levels(test_final$Gender)
levels(test_final$Gender) <- c("M","F") 

# Change Diagnosis to ASD and TD 
levels(test_final$Diagnosis)
test_final$Diagnosis <- ifelse(test_final$Diagnosis == "A", c("ASD"),c("TD")) 

write.csv(test_final, file = "test_final.csv")

# Load training data
train_final <- read_csv("autism_data.csv")
train_final <- train_final[,-1]

train_final$Gender <- as.factor(train_final$Gender)
levels(train_final$Gender)
train_final$Gender <- ifelse(train_final$Gender == "F","M","F")
colnames(train_final)[1] <- "SUBJ"# Now the this colname is the same as in the test set (just in case)
```

### Exercise 1) Testing model performance

How did your models from last time perform? In this exercise, you have to compare the results on the training data () and on the test data. Report both of them. Compare them. Discuss why they are different.

- recreate the models you chose last time (just write the model code again and apply it to your training data (from the first assignment))
- calculate performance of the model on the training data: root mean square error is a good measure. (Tip: google the function rmse())
- create the test dataset (apply the code from assignment 1 part 1 to clean up the 3 test datasets)
- test the performance of the models on the test data (Tips: google the functions "predict()")
- optional: predictions are never certain, can you identify the uncertainty of the predictions? (e.g. google predictinterval())

formatting tip: If you write code in this document and plan to hand it in, remember to put include=FALSE in the code chunks before handing in.

```{r, include=FALSE}
# Recreate model from last time 
#int_int_quad_model = lmer(CHI_MLU ~ VISIT*Diagnosis+I(VISIT^2)*Diagnosis + (1+VISIT+I(VISIT^2)|SUBJ),train_final, REML=FALSE) 

# Calculate performance on the training data (MSE)
#rmse(train_final$CHI_MLU[!is.na(train_final$CHI_MLU)],predict(int_quad_model)) #0.2944186

# Create the test dataset (snydt hjemmefra og lavet det længere oppe :o))
test_final <- read.csv("test_final.csv")
test_final <- test_final[,-1]

# Test performance of the models on the test data (predict()
#rmse(test_final$CHI_MLU[!is.na(test_final$CHI_MLU)],predict(int_quad_model,test_final[!is.na(test_final$CHI_MLU),],allow.new.level=TRUE)) #0.7304555 # Why do we choose all columns in the [] after !is.na?

```

[HERE GOES YOUR ANSWER]
The model predictions are worse when using the test set (MSE=0.73) than the training set (MSE=0.29). Intuitively, this makes sense since it was trained on the training set and the test set is new data. MSE is the average error the models makes when predicting CHI_MLU. Thus, the lower MSE, the lower average error in predictions. 

### Exercise 2) Model Selection via Cross-validation (N.B: ChildMLU!)

One way to reduce bad surprises when testing a model on new data is to train the model via cross-validation. 

In this exercise, you have to use cross-validation to calculate the predictive error of your models and use this predictive error to select the best possible model.

- Use cross-validation to compare your model from last week with the basic model (Child MLU as a function of Time and Diagnosis, and don't forget the random effects!)
- (Tips): google the function "createFolds";  loop through each fold, train both models on the other folds and test them on the fold)
```{r,include=FALSE}
set.seed=1
folds <- createFolds(unique(train_final$SUBJ),5)
rmse_train = NULL
rmse_test = NULL
n=1 

for (f in folds){
  train = subset(train_final,!(SUBJ %in% f))
  test = subset(train_final,(SUBJ %in% f))
  model = lmer(CHI_MLU ~ VISIT + I(VISIT^2) + Diagnosis + (1+VISIT+I(VISIT^2)|SUBJ),train, REML=FALSE)
  rmse_train[n] = rmse(train$CHI_MLU[!is.na(train$CHI_MLU)],predict(model)) 
  rmse_test[n] = rmse(test$CHI_MLU[!is.na(test$CHI_MLU)],predict(model,test[!is.na(test$CHI_MLU),],allow.new.level=TRUE))
  n=n+1
}

# split into train and test, fold f is test, rest is training 
# fit the model to train and test
# attach MSE for both in dataframe

rmse_quad <- cbind(rmse_train,rmse_test)
mean(rmse_quad[,1]) # 0.2887822
mean(rmse_quad[,2]) # 0.8271767
# We take the mean here which is not correct - however, Malte mentioned that this is not as big a concern in this assignment. However, we changed it in assignment 3 (power) where this is a big concern.
```

Trying cross-validation for a linear model
```{r,include=FALSE}
rmse_train = NULL
rmse_test = NULL
n=1 

for (f in folds){
  train = subset(train_final,!(SUBJ %in% f))
  test = subset(train_final,(SUBJ %in% f))
  model = lmer(CHI_MLU ~ VISIT+ Diagnosis + ADOS1 + nonverbalIQ1 + verbalIQ1 + (1+VISIT|SUBJ),train, REML=FALSE)
  rmse_train[n] = rmse(train$CHI_MLU[!is.na(train$CHI_MLU)],predict(model)) 
  rmse_test[n] = rmse(test$CHI_MLU[!is.na(test$CHI_MLU)],predict(model,test[!is.na(test$CHI_MLU),],allow.new.level=TRUE))
  n=n+1
}

# split into train and test, fold f is test, rest is training 
# fit the model to train and test
# attach MSE for both in dataframe

rmse_lin1 <- cbind(rmse_train,rmse_test)
mean(rmse_lin1[,1]) # 0.3477572
mean(rmse_lin1[,2]) # 0.6379023

```

Crossvalidation for best found model from part1_2, int_quad_model:
```{r,include=FALSE,eval=FALSE}
rmse_train = NULL
rmse_test = NULL
n=1 

for (f in folds){
  train = subset(train_final,!(SUBJ %in% f))
  test = subset(train_final,(SUBJ %in% f))
  model =  lmer(CHI_MLU ~ VISIT*Diagnosis+I(VISIT^2)*Diagnosis + (1+VISIT|SUBJ),train, REML=FALSE)
  rmse_train[n] = rmse(train$CHI_MLU[!is.na(train$CHI_MLU)],predict(model)) 
  rmse_test[n] = rmse(test$CHI_MLU[!is.na(test$CHI_MLU)],predict(model,test[!is.na(test$CHI_MLU),],allow.new.level=TRUE))
  n=n+1
}

rmse_12 <- cbind(rmse_train,rmse_test)
mean(rmse_12[,1]) # 0.7770193
mean(rmse_12[,2]) # 0.7618531



cub_model_scale = lmer(CHI_MLU ~ Diagnosis + VISIT_SCALE + I(VISIT_SCALE^2) + I(VISIT_SCALE^3) + (1+VISIT_SCALE+I(VISIT_SCALE^2)+I(VISIT_SCALE^3)|SUBJ), autism_data, REML = "FALSE") # still, fails to converge
int_cub_model_scale = lmer(CHI_MLU ~ Diagnosis*(VISIT_SCALE + I(VISIT_SCALE^2) + I(VISIT_SCALE^3)) + (1+VISIT_SCALE+I(VISIT_SCALE^2)+I(VISIT_SCALE^3)|SUBJ), autism_data, REML = "FALSE")  
```

CV for another model:
```{r,include=FALSE,eval=FALSE}
rmse_train = NULL
rmse_test = NULL
n=1 

for (f in folds){
  train = subset(train_final,!(SUBJ %in% f))
  test = subset(train_final,(SUBJ %in% f))
  model = lmer(CHI_MLU ~ VISIT + Diagnosis + MOT_MLU + I(VISIT^2)+(1+VISIT+ I(VISIT^2)|SUBJ)+ (1|Gender), train, REML = FALSE)
  rmse_train[n] = rmse(train$CHI_MLU[!is.na(train$CHI_MLU)],predict(model)) 
  rmse_test[n] = rmse(test$CHI_MLU[!is.na(test$CHI_MLU)],predict(model,test[!is.na(test$CHI_MLU),],allow.new.level=TRUE))
  n=n+1
}

rmse_1 <- cbind(rmse_train,rmse_test)
mean(rmse_1[,1]) # 0.2786693
mean(rmse_1[,2]) # 0.7783159

```

Simplest, linear model:
```{r,include=FALSE,eval=FALSE}
rmse_train = NULL
rmse_test = NULL
n=1 

for (f in folds){
  train = subset(train_final,!(SUBJ %in% f))
  test = subset(train_final,(SUBJ %in% f))
  model = lmer(CHI_MLU ~ VISIT + Diagnosis + (1+VISIT|SUBJ), train, REML = FALSE)
  rmse_train[n] = rmse(train$CHI_MLU[!is.na(train$CHI_MLU)],predict(model)) 
  rmse_test[n] = rmse(test$CHI_MLU[!is.na(test$CHI_MLU)],predict(model,test[!is.na(test$CHI_MLU),],allow.new.level=TRUE))
  n=n+1
}

rmse_2 <- cbind(rmse_train,rmse_test)
mean(rmse_2[,1]) # 0.6899543
mean(rmse_2[,2]) # 0.7715127

```

Another model:
```{r,include=FALSE,eval=FALSE}
rmse_train = NULL
rmse_test = NULL
n=1 

for (f in folds){
  train = subset(train_final,!(SUBJ %in% f))
  test = subset(train_final,(SUBJ %in% f))
  model = lmer(CHI_MLU ~ VISIT + Diagnosis + MOT_MLU + ADOS1 + types_CHI + tokens_CHI + I(VISIT^2)+(1+VISIT+ I(VISIT^2)|SUBJ), train, REML = FALSE)
  rmse_train[n] = rmse(train$CHI_MLU[!is.na(train$CHI_MLU)],predict(model)) 
  rmse_test[n] = rmse(test$CHI_MLU[!is.na(test$CHI_MLU)],predict(model,test[!is.na(test$CHI_MLU),],allow.new.level=TRUE))
  n=n+1
}

rmse_3 <- cbind(rmse_train,rmse_test)
mean(rmse_3[,1]) # 0.2694757
mean(rmse_3[,2]) # 0.492886
```

Last model:
```{r,include=FALSE,eval=FALSE}
rmse_train = NULL
rmse_test = NULL
n=1 

for (f in folds){
  train = subset(train_final,!(SUBJ %in% f))
  test = subset(train_final,(SUBJ %in% f))
  model = lmer(CHI_MLU ~ Diagnosis + (VISIT+I(VISIT^2)) + ADOS1 + verbalIQ1 + (1+VISIT|SUBJ),train, REML=FALSE)
  rmse_train[n] = rmse(train$CHI_MLU[!is.na(train$CHI_MLU)],predict(model)) 
  rmse_test[n] = rmse(test$CHI_MLU[!is.na(test$CHI_MLU)],predict(model,test[!is.na(test$CHI_MLU),],allow.new.level=TRUE))
  n=n+1
}

rmse_last <- cbind(rmse_train,rmse_test)
mean(rmse_3[,1]) # 0.2718063
mean(rmse_3[,2]) # 0.4685206
```

```{r Best model,echo=FALSE}
best_model_cv <- lmer(CHI_MLU ~ Diagnosis + (VISIT+I(VISIT^2)) + ADOS1 + verbalIQ1 + (1+VISIT|SUBJ),train_final, REML=FALSE)
```


Which model is better at predicting new data: the one you selected last week or the one chosen via cross-validation this week?

- Test both of them on the test data.
- Report the results and comment on them.

- Now try to find the best possible predictive model of ChildMLU, that is, the one that produces the best cross-validated results.

- Bonus Question 1: What is the effect of changing the number of folds? Can you plot RMSE as a function of number of folds?
- Bonus Question 2: compare the cross-validated predictive error against the actual predictive error on the test data


[HERE GOES YOUR ANSWER]
We tested several models on the training and test data and calculated the MSE for those.
The best model found in the last assignment (only by looking at AIC) was: CHI_MLU ~ VISIT*Diagnosis+I(VISIT^2)*Diagnosis + (1+VISIT|SUBJ).

The results from this model was MSE for the training data = 0.78 and MSE for the test data = 0.76. 
Thus, it actually performed a bit better on the test data than on the training data. 

The simple model (CHI_MLU ~ VISIT + Diagnosis + (1+VISIT|SUBJ)) performed a bit better on the training data (MSE = 0.69) and slightly worse on the test data (MSE = 0.77).

The best model found via cross-validation was the following: 
CHI_MLU ~ VISIT + Diagnosis + MOT_MLU + ADOS1 + types_CHI + tokens_CHI + I(VISIT^2)+(1+VISIT+ I(VISIT^2)|SUBJ)+ (1|Gender)
The results from this model was MSE for the training data = 0.26 and MSE for the test data = 0.49.
Thus, it performed better on the training data than on the test data. However, both results are still lower, and thus, better, than the results from the best model from last assignment. BUT this model leaks information in the sense that we uses both the types and tokens of the child + the mother's MLU to predict the child's MLU. 
Leakage: when information from outside the training dataset is used to create the model.

Thus, the best model (without leakage) is: CHI_MLU ~ Diagnosis*(VISIT+I(VISIT^2)) + ADOS1 + verbalIQ1 + (1+VISIT|SUBJ). Results for this model was MSE_train = 0.27, MSE_test = 0.47. 

### Exercise 3) Assessing the single child

Let's get to business. This new kiddo - Bernie - has entered your clinic. This child has to be assessed according to his group's average and his expected development.

Bernie is one of the six kids in the test dataset, so make sure to extract that child alone for the following analysis.

You want to evaluate:
- how does the child fare in ChildMLU compared to the average TD child at each visit? Define the distance in terms of absolute difference between this Child and the average TD.
(Tip: recreate the equation of the model: Y=Intercept+BetaX1+BetaX2, etc; input the average of the TD group  for each parameter in the model as X1, X2, etc.). (we did this when handing in the first round, but for the exam, we just use the predict function which does the same)

- how does the child fare compared to the model predictions at Visit 6? Is the child below or above expectations? (tip: use the predict() function on Bernie's data only and compare the prediction with the actual performance of the child)

```{r,echo=FALSE}
TDsubset <- subset(train_final,Diagnosis == "TD")

TD_ADOS <- mean(TDsubset$ADOS1)
TD_types_CHI <- mean(TDsubset$types_CHI,na.rm=TRUE)
TD_tokens_CHI <- mean(TDsubset$tokens_CHI,na.rm=TRUE)
```

```{r old, eval=FALSE,echo=FALSE}
summary(best_model_cv) 
# General formula for best model chosen via crossvalidation:
#TD = -1.234e-01 + 1.740e-02 + 2.624e-01 + 5.394e-03 + 7.258e-03 + 2.613e-04 + 6.479e-02*x + 2.770e-03^2*x

# Evaluate how the child fare in CHI_MLU compared to the average TD child at each visit. Define the distance in terms of absolute difference between this Child and the average TD.
# (Tip: recreate the equation of the model: Y=Intercept+BetaX1+BetaX2, etc; input the average of the TD group for each parameter in the model as X1, X2, etc.).

TD1 = (-1.501e-01) + 2.660e-02*1 + 1.962e-01*1 + 2.322e-01*TD_MOT_MLU + 1.098e-02*TD_ADOS + 8.006e-03*TD_types_CHI + 1.932e-04*TD_tokens_CHI + 2.615e-03*(1^2)
TD1 #2.176558
BernieV1 = subset(Bernie,VISIT==1)
BernieV1$CHI_MLU #1.984456
V1Diff = BernieV1$CHI_MLU - TD1
V1Diff # -0.1921019

TD2 = (-1.501e-01) + 2.660e-02*2 + 1.962e-01*1 + 2.322e-01*TD_MOT_MLU + 1.098e-02*TD_ADOS + 8.006e-03*TD_types_CHI + 1.932e-04*TD_tokens_CHI + 2.615e-03*(2^2)
TD2 # 2.211003
BernieV2 = subset(Bernie,VISIT==2)
BernieV2$CHI_MLU # CHI_MLU: 2.544444
V2Diff = BernieV2$CHI_MLU - TD2
V2Diff # 0.3334416

TD3 = (-1.501e-01) + 2.660e-02*3 + 1.962e-01*1 + 2.322e-01*TD_MOT_MLU + 1.098e-02*TD_ADOS + 8.006e-03*TD_types_CHI + 1.932e-04*TD_tokens_CHI + 2.615e-03*(3^2)
TD3 # 2.250678
BernieV3 = subset(Bernie,VISIT==3)
BernieV3$CHI_MLU # CHI_MLU: 3.353191
V3Diff = BernieV3$CHI_MLU - TD3
V3Diff # 1.102514 

TD4 = (-1.501e-01) + 2.660e-02*4 + 1.962e-01*1 + 2.322e-01*TD_MOT_MLU + 1.098e-02*TD_ADOS + 8.006e-03*TD_types_CHI + 1.932e-04*TD_tokens_CHI + 2.615e-03*(4^2)
TD4 # 2.295583
BernieV4 = subset(Bernie,VISIT==4)
BernieV4$CHI_MLU # CHI_MLU: 3.183099
V4Diff = BernieV4$CHI_MLU - TD4
V4Diff # 0.8875158

TD5 = (-1.501e-01) + 2.660e-02*5 + 1.962e-01*1 + 2.322e-01*TD_MOT_MLU + 1.098e-02*TD_ADOS + 8.006e-03*TD_types_CHI + 1.932e-04*TD_tokens_CHI + 2.615e-03*(5^2)
TD5 # 2.345718
BernieV5 = subset(Bernie,VISIT==5)
BernieV5$CHI_MLU # CHI_MLU: 3.173252
V5Diff = BernieV5$CHI_MLU - TD5
V5Diff # 0.8275344

TD6 = (-1.501e-01) + 2.660e-02*6 + 1.962e-01*1 + 2.322e-01*TD_MOT_MLU + 1.098e-02*TD_ADOS + 8.006e-03*TD_types_CHI + 1.932e-04*TD_tokens_CHI + 2.615e-03*(6^2)
TD6 # 2.401083
BernieV6 = subset(Bernie,VISIT==6)
BernieV6$CHI_MLU # CHI_MLU: 3.448413
V6Diff = BernieV6$CHI_MLU - TD6
V6Diff # 1.04733
```


```{r,include=FALSE}
# Extract Bernie 
Bernie <- filter(test_final,SUBJ == "Bernie")

# Predict the average TD at each visit
predictions = predict(best_model_cv,TDsubset,allow.new.levels=TRUE)

TDsubset$predictions <- predictions

TDV1 <- filter(TDsubset,VISIT ==1)
TDV2 <- filter(TDsubset,VISIT ==2)
TDV3 <- filter(TDsubset,VISIT ==3)
TDV4 <- filter(TDsubset,VISIT ==4)
TDV5 <- filter(TDsubset,VISIT ==5)
TDV6 <- filter(TDsubset,VISIT ==6)

tdpred <- as.data.frame(matrix(nrow=1,ncol=6))
tdpred$V1 <- mean(TDV1$predictions,na.rm=TRUE)
tdpred$V2 <- mean(TDV2$predictions)
tdpred$V3 <- mean(TDV3$predictions)
tdpred$V4 <- mean(TDV4$predictions)
tdpred$V5 <- mean(TDV5$predictions)
tdpred$V6 <- mean(TDV6$predictions)
tdpred

tdpred <- gather(tdpred)

# calculating absolute difference from average TD to Bernie 
Bernie[1,12] - tdpred[1,2] # 0.6266522
Bernie[2,12] - tdpred[2,2] # 0.6904064
Bernie[3,12] - tdpred[3,2] # 1.071809
Bernie[4,12] - tdpred[4,2] # 0.5806549
Bernie[5,12] - tdpred[5,2] # 0.3399987
Bernie[6,12] - tdpred[6,2] # 0.4485406

# predict for Bernie
all_data_except_BernieV6 <- rbind(train_final,test_final)
all_data_except_BernieV6 <- filter(all_data_except_BernieV6, !(SUBJ == "Bernie" & VISIT == 6))

best_model_cv_alldataexceptBernieV6 <- lmer(CHI_MLU ~ Diagnosis + (VISIT+I(VISIT^2)) + ADOS1 + verbalIQ1 + (1+VISIT|SUBJ),all_data_except_BernieV6, REML=FALSE)

berniepred <- as.data.frame(predict(best_model_cv,newdata=Bernie,allow.new.levels=TRUE))
bernepred <- gather(berniepred)

tdpred$berniepred <- berniepred
colnames(tdpred) <- c("VISIT","tdpred","berniepred")

tdpred$absolutdifference <- tdpred$berniepred - tdpred$tdpred # absolute difference at V6: 0.12
```

[HERE GOES YOUR ANSWER]
Bernie has a higher MLU than an average TD at every visit:
Difference at V1: 0.63
Difference at V2: 0.69
Difference at V3: 1.07
Differnece at V4: 0.58
Difference at V5: 0.34
Difference at V6: 0.45 

The difference gets slightly smaller at every visit. Thus, at every visit Bernie's MLU is higher than the average typical developing child, i.e. his MLU is higher than expected than if we were developing in a typical manner. Generallt, however, his MLU does not develop as much as the typical developing children's MLU, i.e. the distance is getting smaller (generally. 
When predicting Bernie's sixth visit from all data except his 6th visit, he is a bit over the expected MLU (0.12 MLU to be exact) if he was TD. 

Plots:

```{r, echo=FALSE}
# plotting model predictions - MALTE
TD = filter(train_final, Diagnosis == "TD")
TD = TD[-1,]
# make fake data to represent the average
expand.grid(VISIT = 1:6,
            SUBJ = "TD",
            Diagnosis = "TD",
            ADOS1 = mean(TD$ADOS1),
            verbalIQ1 = mean(TD$verbalIQ1)) %>%
  # make predictions based on this fake data
  mutate(CHI_MLU = predict(best_model_cv, newdata = ., allow.new.levels = TRUE)) %>%
  # plot those predictions
  ggplot(aes(VISIT, CHI_MLU, colour = "Model Average for \n Typically Developing Children \n (child MLU ~ diagnosis * (visit + visit^2) + \n ADOS + verbal IQ + (visit + visit^2|subject))")) +
  geom_line() +
  geom_point(aes(colour = "Data Points for Bernie"), data = Bernie) +
  labs(x = "Visit", y = "Child MLU", title = "Model predictions vs. Bernie") +
  scale_colour_brewer(palette = "Dark2") +
  theme(legend.title=element_blank())

#plotting Bernie predictions
Bernie %>%
  # make predictions based on this fake data
  mutate(CHI_MLU = predict(best_model_cv, newdata = ., allow.new.levels = TRUE)) %>%
  # plot those predictions
  ggplot(aes(VISIT, CHI_MLU, colour = "Model Predictions for Bernie \n (child MLU ~ diagnosis * (visit + visit^2) + \n ADOS + verbal IQ + (visit + visit^2|subject))")) +
  geom_line() +
  geom_point(aes(colour = "Data Points for Bernie"), data = Bernie) +
  labs(x = "Visit", y = "Child MLU", title = "Bernie vs. Bernie predictions") +
  scale_colour_brewer(palette = "Dark2") + theme(legend.title=element_blank())

#plotting Bernie with model including all data except his 6th visit
Bernie %>%
  # make predictions based on this fake data
  mutate(CHI_MLU = predict(best_model_cv_alldataexceptBernieV6, newdata = ., allow.new.levels = TRUE)) %>%
  # plot those predictions
  ggplot(aes(VISIT, CHI_MLU, colour = "Model Predictions for Bernie \n with all data except Bernie's \n V6  (child MLU ~ diagnosis* \n (visit + visit^2) + ADOS + \n verbal IQ + (visit + visit^2|subject))")) +
  geom_line() +
  geom_point(aes(colour = "Data Points for Bernie"), data = Bernie) +
  labs(x = "Visit", y = "Child MLU", title = "Bernie with model including all data except his V6") +
  scale_colour_brewer(palette = "Dark2") + theme(legend.title=element_blank())

```

